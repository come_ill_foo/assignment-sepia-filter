#ifndef _LOG_H
#define _LOG_H

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#include "util.h"

void usage( const char* program, const char* msg, ... );

#define DEF_IMPL_LOG_LEVEL( name, COLOUR )               \
static inline void name( const char* msg, ... ) {        \
  va_list args;                                          \
  va_start( args, msg );                                 \
  fprintf( stderr, COLOUR #name ": " ANSI_COLOR_RESET ); \
  vfprintf( stderr, msg, args );                         \
  va_end( args );                                        \
}

#define DEF_STUB_LOG_LEVEL( name )                \
static inline void name( const char* msg, ... ) { \
  ( void ) msg;                                   \
}

#ifdef LOG_LEVEL_VERBOSE
  DEF_IMPL_LOG_LEVEL( info,  ANSI_COLOR_YELLOW )
#else
  DEF_STUB_LOG_LEVEL( info )
#endif

#ifdef LOG_LEVEL_VERBOSE
  DEF_IMPL_LOG_LEVEL( alert, ANSI_COLOR_RED    )
#else
  DEF_STUB_LOG_LEVEL( alert )
#endif

#define INFO_FUNC_STARTED  info( "%s: started\n", __func__ )
#define INFO_FUNC_FINISHED info( "%s: finished\n", __func__ )

#endif /* _LOG_H */