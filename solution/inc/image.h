#ifndef _IMAGE_H
#define _IMAGE_H

#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>


struct pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image image_create( uint64_t width, uint64_t height );

void image_destroy( struct image img );

/* makes copy of image which is rotated to 90 degrees */
struct image rotate( struct image const source );

/* makes copy of image which is sepied */
struct image sepia( struct image const source );

/* also like sepia but use simd instructions */
struct image sepia_arch( struct image const source );

#endif /* _IMAGE_H */