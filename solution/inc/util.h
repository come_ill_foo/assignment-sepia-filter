#ifndef _UTIL_H_
#define _UTIL_H_

#include <stdint.h>
#include <string.h>
#include <stdbool.h>

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

#define STRINGIFY( x ) #x
#define STR( x ) STRINGIFY( x )


static inline bool string_ends_with( const char * str, const char * suffix ) {
  const size_t str_len = strlen(str);
  const size_t suffix_len = strlen(suffix);
  return 
    ( str_len >= suffix_len ) &&
    ( 0 == strcmp( str + ( str_len-suffix_len ), suffix ) );
}

#endif /* _UTIL_H_ */