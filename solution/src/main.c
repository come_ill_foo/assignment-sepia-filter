#include <stdio.h>

#include "log.h"
#include "image.h"
#include "image_arch.h"

extern const char* img_read_msgs[];

extern const char* img_write_msgs[];


int main( int argc, char** argv ) {
  INFO_FUNC_STARTED;

  if ( argc != 3 ) {
    alert( "invalid arguments number provided\n" );
    usage( argv[ 0 ], "<source-image> <modified-image>\n" );
    return 1;
  }

  // get from
  struct image source = {0};
  enum read_status in_status = from_fpath( argv[ 1 ], &source );

  if ( in_status != READ_OK ) {
    alert( img_read_msgs[ in_status ] ); return 1; }
  info( img_read_msgs[ in_status ] );

  // apply filter
  struct image dest =
  #ifdef _simd
    sepia_arch( source );
  #else
    sepia( source );
  #endif
  image_destroy( source );

  // set to
  enum write_status out_status = to_fpath( argv[ 2 ], &dest );

  image_destroy( dest );

  if ( out_status != WRITE_OK ) {
    alert( img_write_msgs[ out_status ] ); return 1; }
  info( img_write_msgs[ out_status ] );

  INFO_FUNC_FINISHED;
  return 0;
}
