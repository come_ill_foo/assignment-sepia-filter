section .text
sepia_core_col_0: dd 0.393, 0.349, 0.272, 0.393 ; 4 * 4 = 16 bytes
sepia_core_col_1: dd 0.769, 0.686, 0.543, 0.769 ; 4 * 4 = 16 bytes
sepia_core_col_2: dd 0.189, 0.168, 0.131, 0.189 ; 4 * 4 = 16 bytes

global sepia_apply_core_to_one_arch

; rdi = r, rsi = g, rdx = b
do_sepia_apply_core_to_one_arch:
  movdqa xmm3, [ rel sepia_core_col_0 ]
  movdqa xmm4, [ rel sepia_core_col_1 ]
  movdqa xmm5, [ rel sepia_core_col_2 ]
  movdqu xmm0, [rdi] ; get all red colours
  movdqu xmm1, [rsi] ; get all green colours
  movdqu xmm2, [rdx] ; get all blue colours
  mulps  xmm0, xmm3 ; r1r1r1r2 * c11c21c31c11
  mulps  xmm1, xmm4 ; g1g1g1g2 * c12c22c32c12
  mulps  xmm2, xmm5 ; b1b1b1b2 * c13c23c33c13
  addps  xmm0, xmm1
  addps  xmm0, xmm2
  movdqu [rdi], xmm0
  ret

; rdi - float[ static 12 ]
sepia_apply_core_to_one_arch:
  lea rsi, [ rdi + 16 ] ; get address of greens
  lea rdx, [ rdi + 32 ] ; get address of blues
  push rdi
  push rsi
  push rdx
  call do_sepia_apply_core_to_one_arch
  pop rdx
  pop rsi
  pop rdi
  ret
