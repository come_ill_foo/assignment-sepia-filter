#include "log.h"

void usage( const char* program, const char* msg, ... ) {
  va_list args;
  va_start( args, msg );
  fprintf( stderr, ANSI_COLOR_GREEN "usage: " ANSI_COLOR_RESET "%s ", program );
  vfprintf( stderr, msg, args );
  va_end( args ); 
}