#include "image_arch.h"

/* array of read error messages */
const char* img_read_msgs[] = {
  [ READ_OK ]                = "source-image successfully has been read\n",
  [ READ_INVALID_HEADER ]    = "encountered invalid format header\n",
  [ READ_INVALID_BITS ]      = "encountered invalid bit count ( biBitCount )\n",
  [ READ_INVALID_BITMAP ]    = "bitmap isnot matching image header\n",
  [ READ_INVALID_SIGNATURE ] = "encountered invalid signature ( bfType )\n",
  [ READ_NOT_SUPPORTED ]     = "this format is not supported for reading\n",
  [ READ_INVALID_PATH ]      = "invalid source-image path provided\n",
  [ READ_CANT_OPEN_FILE ]    = "cant open source-image\n",
  [ READ_CANT_CLOSE_FILE ]   = "cant close source-image\n"
};

/* array of write error messages */
const char* img_write_msgs[] = {
  [ WRITE_OK ]                = "modified-image successfully has been written\n",
  [ WRITE_HEADER_ERROR ]      = "encountered errors while saving image header\n",
  [ WRITE_BITMAP_ERROR ]      = "encountered errors while saving image content\n",
  [ WRITE_NOT_SUPPORTED ]     = "this format is not supported for writing\n",
  [ WRITE_INVALID_PATH ]      = "invalid modified-image path provided\n",
  [ WRITE_CANT_OPEN_FILE ]    = "cant open modified-image\n",
  [ WRITE_CANT_CLOSE_FILE ]   = "cant close modified-image\n"
};


/* stub for read */
static enum read_status from_stub( FILE* const in, struct image* const img ) {
  ( void ) in; ( void ) img; // suppress warnings from unused variables
  return READ_NOT_SUPPORTED;
}

/* stub for write */
static enum write_status to_stub( FILE* const in, const struct image* img ) {
  ( void ) in; ( void ) img; // suppress warnings from unused variables
  return WRITE_NOT_SUPPORTED;
}


const struct img_descr formats[] = {
  [ IMG_BMP ] = { ".bmp", from_bmp, to_bmp },
  [ IMG_PNG ] = { ".png", from_stub, to_stub },
  [ IMG_JPG ] = { ".jpg", from_stub, to_stub },
  [ IMG_DEF ] = { ""    , from_stub, to_stub }
};


static uint64_t count_padding( uint64_t width ) { return width % 4; }


static enum read_status validate_bmp_header( struct bmp_header header ) {
  INFO_FUNC_STARTED;
  bool is_header_invalid = false;

  info( "bfType:\ttest:\texpected: " STR( BMP_HEADER_VALID_BFTYPE ) "; actual: 0x%x\n", header.bfType );
  is_header_invalid = is_header_invalid || ( header.bfType != BMP_HEADER_VALID_BFTYPE ); // bfType = "BM"
  if ( is_header_invalid ) return READ_INVALID_SIGNATURE;

  // should also include check if biSizeImage is not zero otherwise it point to uncompressed RGB bitmaps
  info( "bfileSize:\ttest:\texpected: %" PRIu32 "; actual: %" PRIu32 "\n", header.biSizeImage + header.bOffBits, header.bfileSize );
  is_header_invalid = is_header_invalid || ( ( header.biSizeImage != 0 ) && header.bfileSize != ( header.biSizeImage + header.bOffBits ) );
  if ( is_header_invalid ) return READ_INVALID_HEADER;

  // check bfReserved ommitted because as english wikipedia says:
  // its actual value depends on the application that creates the image,
  // if created manually can be 0

  info( "bOffBits:\ttest:\texpected: %zu; actual: %" PRIu32 "\n", sizeof( struct bmp_header ), header.bOffBits );
  is_header_invalid = is_header_invalid || ( header.bOffBits != sizeof( struct bmp_header ) );
  if ( is_header_invalid ) return READ_INVALID_HEADER;
  // ?maybe useless or destructive check

  info( "biSize:\ttest:\texpected: " STR( BITMAPINFOHEADER_SIZE ) "; actual: %" PRIu32 "\n", header.biSize );
  is_header_invalid = is_header_invalid || ( header.biSize != BITMAPINFOHEADER_SIZE );
  if ( is_header_invalid ) return READ_INVALID_HEADER;
  // ?maybe useless or destructive check

  info( "biPlanes:\ttest:\texpected: " STR( BMP_BI_PLANES ) "; actual: %" PRIu16 "\n", header.biPlanes );
  is_header_invalid = is_header_invalid || ( header.biPlanes != BMP_BI_PLANES );
  if ( is_header_invalid ) return READ_INVALID_HEADER;

  info( "biBitCount:\ttest:\texpected: " STR( BMP_BI_BIT_COUNT ) "; actual: %" PRIu16 "\n", header.biBitCount );
  is_header_invalid = is_header_invalid || ( header.biBitCount != BMP_BI_BIT_COUNT );
  if ( is_header_invalid ) return READ_INVALID_BITS;

  info( "biCompression:\ttest:\texpected: %x; actual: %x\n", BI_RGB, header.biCompression );
  is_header_invalid = is_header_invalid || ( header.biCompression != BI_RGB );
  if ( is_header_invalid ) return READ_INVALID_HEADER;

  // should also include check if biSizeImage is not zero otherwise it point to uncompressed RGB bitmaps
  // info( "+ checking biSizeImage\n" );
  // is_header_invalid = is_header_invalid || ( ( head.biSizeImage != 0 ) && head.biSizeImage != ( head.biHeight * ( head.biWidth * sizeof( struct pixel ) + count_padding( head.biWidth ) ) ) );
  // ?maybe useless or destructive check ( really useless 'cause of tests in image-rotation )

  // check of biXPelsPerMeter and biYPelsPerMeter ommitted
  // because its value depends on display

  // check of biClrUsed and biClrImportant also ommitted
  // these values also depends on hardware specification
  INFO_FUNC_FINISHED;
  return is_header_invalid? READ_INVALID_HEADER : READ_OK;
}

static enum read_status fread_bmp_header( FILE* const in, struct bmp_header* header ) {
  const size_t number_of_read_headers = fread( header, sizeof( struct bmp_header ), 1, in );
  info( "number of read bmp headers: [ %zu ]\n", number_of_read_headers );

  if ( number_of_read_headers != 1 ) return READ_INVALID_HEADER;
  info( "found at least one bmp header\n" );
  return READ_OK;
}

static enum read_status fread_bmp_image( FILE* const in, struct image* const img, const uint64_t width, const uint64_t height ) {
  INFO_FUNC_STARTED;

  *img = image_create( width, height );
  const uint64_t padding = count_padding( img->width );
  info( "img: [ width: %" PRIu64 " ; height: %" PRIu64 " ]; padding: [ %" PRIu64 " ]\n", img->width, img->height, padding );

  for ( uint64_t i = 0; i < img->height; ++i ) {
    const size_t number_of_read_pixels = fread( img->data + i * img->width, sizeof( struct pixel ), img->width, in );
    info( "< on row[ %" PRIu64 " ]\tread [ %zu ] pixels\n", i, number_of_read_pixels );

    if ( number_of_read_pixels < img->width ) {
      image_destroy( *img );
      alert( "read less than [ %" PRIu64 " ] pixels: actual: [ %zu ]; finishing reading...\n", img->width, number_of_read_pixels );
      return READ_INVALID_BITMAP;
    }

    // go to the next row skipping padding
    if ( fseek( in, padding, SEEK_CUR ) ) {
      image_destroy( *img );
      alert( "can't seek the next row; finishing reading...\n" );
      return READ_INVALID_BITMAP;
    }
  }

  INFO_FUNC_FINISHED;
  return READ_OK;
}

enum read_status from_bmp( FILE* const in, struct image* const img ) {
  INFO_FUNC_STARTED;
  enum read_status status = READ_OK;
  struct bmp_header header = {0};
  status = fread_bmp_header( in, &header );
  if ( status != READ_OK ) return status;

  info( "validate BMP header\n" );
  status = validate_bmp_header( header );
  if ( status != READ_OK ) return status;
  info( "found a valid BMP header\n" );

  info( "seek the start of the image's bitmap\n" );
  if ( fseek( in, header.bOffBits, SEEK_SET ) ) return READ_INVALID_SIGNATURE;
  info( "seeked successfully\n" );

  status = fread_bmp_image( in, img, header.biWidth, header.biHeight );

  INFO_FUNC_FINISHED;
  return status;
}

enum read_status from_format( FILE* const in, struct image* const img, const enum img_format format ) {
  return formats[ format ].from( in, img );
}

static enum img_format define_format( const char* source ) {
  if ( string_ends_with( source, formats[ IMG_BMP ].ext ) )
    return IMG_BMP;
  else if ( string_ends_with( source, formats[ IMG_PNG ].ext ) )
    return IMG_PNG;
  else if ( string_ends_with( source, formats[ IMG_JPG ].ext ) )
    return IMG_JPG;
  else
    return IMG_DEF;
}

enum read_status from_fpath( const char* source, struct image* const img ) {
  INFO_FUNC_STARTED;
  if ( source == NULL ) return READ_INVALID_PATH;

  FILE* fsource = fopen( source, "rb" );
  if ( fsource == NULL ) return READ_CANT_OPEN_FILE;
  info( "file [ %s ] successfully opened\n", source );

  enum img_format fformat = define_format( source );

  enum read_status status = from_format( fsource, img, fformat );
  info( "deserializing completed with status code: [ %zu ]\n", status );

  if ( status != READ_OK ) {
    if ( fclose( fsource ) == EOF ) return READ_CANT_CLOSE_FILE;
    return status;
  }

  if ( fclose( fsource ) == EOF ) {
    image_destroy( *img );
    return READ_CANT_CLOSE_FILE;
  }
  info( "file [ %s ] successfully closed\n", source );

  INFO_FUNC_FINISHED;
  return status;
}

static struct bmp_header bmp_header_create( uint32_t width, uint32_t height ) {
  const uint32_t biSizeImage = height * ( width * sizeof( struct pixel ) + count_padding( width ) ); // calculated size of image in bytes
  const uint32_t bOffBits = sizeof( struct bmp_header );

  return ( struct bmp_header ) {
    .bfType         = BMP_HEADER_VALID_BFTYPE, // valid constant "BM" in big-endian
    .bfileSize      = bOffBits + biSizeImage,  // calculated file size in bytes
    .bfReserved     = 0,                       // "can be zero if write manually"
    .bOffBits       = bOffBits,                // the size of bmp header struct in bytes
    .biSize         = BITMAPINFOHEADER_SIZE,   // constant for supported bmp version format
    .biWidth        = width,                   // just the width of image
    .biHeight       = height,                  // just the height of image
    .biPlanes       = BMP_BI_PLANES,           // must be 1 for this bmp version because this parameter kept for backward compatibility
    .biBitCount     = BMP_BI_BIT_COUNT,        // standard bpp
    .biCompression  = BI_RGB,                  // use this constant for uncompressed images
    .biSizeImage    = biSizeImage,             // size of image
    .biClrUsed      = 0,                       // to use full kit of colours
    .biClrImportant = 0                        // to use full kit of colours
  };
}

static enum write_status fwrite_bmp_header( FILE* const out, const struct bmp_header header ) {
  const size_t number_of_written_headers = fwrite( &header, sizeof( struct bmp_header ), 1, out );
  info( "number of written bmp headers: [ %zu ]\n", number_of_written_headers );
  if ( number_of_written_headers != 1 ) return WRITE_HEADER_ERROR;
  info( "written exactly one bmp header\n" );
  return WRITE_OK;
}

static enum write_status fwrite_bmp_image( FILE* const out, const struct image img ) {
  INFO_FUNC_STARTED;
  const uint8_t rubbish[ 3 ] = { 0, 0, 0 };

  uint64_t padding = count_padding( img.width );
  info( "img: [ width: %" PRIu64 " ; height: %" PRIu64 " ]; padding: [ %" PRIu64 " ]\n", img.width, img.height, padding );

  for ( uint32_t i = 0; i < img.height; ++i ) {

    const size_t number_of_written_pixels = fwrite( img.data + i * img.width, sizeof(struct pixel), img.width, out );
    info( "> on row[ %" PRIu64 " ]\twrote [ %zu ] pixels\n", i, number_of_written_pixels );

    if ( number_of_written_pixels < img.width ) {
      image_destroy( img );
      alert( "wrote less than [ %" PRIu64 " ] pixels: actual: [ %zu ]; finishing writing...\n", img.width, number_of_written_pixels );
      return WRITE_BITMAP_ERROR;
    }

    if ( fwrite( rubbish, 1, padding, out ) < ( size_t ) padding ) {
      image_destroy( img );
      alert( "can't align bitmap; finishing writing...\n" );
      return WRITE_BITMAP_ERROR;
    }
  }

  INFO_FUNC_FINISHED;
  return WRITE_OK;
}

enum write_status to_bmp( FILE* const out, struct image const* const img ) {
  INFO_FUNC_STARTED;
  enum write_status status = WRITE_OK;
  struct bmp_header head = bmp_header_create( img->width, img->height );

  info( "writing bmp header into file\n" );
  status = fwrite_bmp_header( out, head );
  if ( status != WRITE_OK ) return status;
  info( "bmp header was successfully written into file\n" );

  status = fwrite_bmp_image( out, *img );
  INFO_FUNC_FINISHED;
  return status;
}

enum write_status to_format( FILE* const out, struct image const* const img, const enum img_format format ) {
  return formats[ format ].to( out, img );
}

enum write_status to_fpath( const char* dest, struct image const* const img ) {
  INFO_FUNC_STARTED;

  if ( dest == NULL ) return WRITE_INVALID_PATH;

  FILE* fdest = fopen( dest, "wb" );
  if ( fdest == NULL ) return WRITE_CANT_OPEN_FILE;
  info( "file [ %s ] successfully opened\n", dest );

  enum img_format fformat = define_format( dest );

  enum write_status status = to_format( fdest, img, fformat );
  info( "serializing completed with status code: [ %zu ]\n", status );

  if ( status != WRITE_OK ) {
    if ( fclose( fdest ) == EOF ) return WRITE_CANT_CLOSE_FILE;
    return status;
  }

  if ( fclose( fdest ) == EOF ) {
    image_destroy( *img );
    return WRITE_CANT_CLOSE_FILE;
  }
  info( "file [ %s ] successfully closed\n", dest );

  INFO_FUNC_FINISHED;
  return status;
}