#include <sys/time.h>
#include <sys/resource.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>

#define LOOP_CYCLES 999

int main( void ) {
  struct rusage r;
  struct timeval start;
  struct timeval end;
  getrusage( RUSAGE_SELF, &r );
  start = r.ru_utime;
  for( uint64_t i = 0; i < LOOP_CYCLES; i++ )
    system( "./sepia_arch ./tests/images/2/lenna.bmp ./tests/images/2/old_lenna.bmp" );
  getrusage( RUSAGE_SELF, &r );
  end = r.ru_utime;
  long res = ( ( end.tv_sec - start.tv_sec ) * 1000000L ) + end.tv_usec - start.tv_usec;
  printf( "   SIMD: Time elapsed in microseconds: %ld\n", res );

  getrusage( RUSAGE_SELF, &r );
  start = r.ru_utime;
  for( uint64_t i = 0; i < LOOP_CYCLES; i++ )
    system( "./sepia ./tests/images/2/lenna.bmp ./tests/images/2/old_lenna.bmp" );
  getrusage( RUSAGE_SELF, &r );
  end = r.ru_utime;
  res = ( ( end.tv_sec - start.tv_sec ) * 1000000L ) + end.tv_usec - start.tv_usec;
  printf( "No SIMD: Time elapsed in microseconds: %ld\n", res );
  return 0;
}
